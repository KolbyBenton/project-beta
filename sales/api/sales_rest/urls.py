from django.urls import path

from .views import (
api_list_salesperson,
api_list_customer,
api_list_sale,
api_delete_salesperson,
api_delete_customer,
api_delete_sale,
api_sales_person_history,
)

urlpatterns = [
    path('salespeople/', api_list_salesperson, name='api_list_salesperson'),
    path('customers/', api_list_customer, name='api_list_customer'),
    path('sales/', api_list_sale, name='api_list_sale'),
    path('salespeople/<str:id>/', api_delete_salesperson, name='api_delete_salesperson'),
    path('customers/<str:id>/', api_delete_customer, name='api_delete_customer'),
    path('sales/<str:id>/', api_delete_sale, name='api_delete_sale'),
    path('sales/history/<str:id>/',api_sales_person_history, name='api_sales_person_history'),
    path('sales/history/',api_sales_person_history, name='api_sales_person_history'),
]
