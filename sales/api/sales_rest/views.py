from django.shortcuts import render
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from .encoders import(
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    try:
        salespersons = Salesperson.objects.get(id=id)
        salespersons.delete()
        return JsonResponse(
            {"message": "Salesperson has been succesfully deleted!"}
        )
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"error": "Salesperson not found!"},
            status=404
        )

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers_list = Customer.objects.all()
        return JsonResponse(
            {"customers_list": customers_list},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    try:
        customers = Customer.objects.get(id=id)
        customers.delete()
        return JsonResponse(
            {"message": "Customer has been succesfully deleted!"}
        )
    except Customer.DoesNotExist:
        return JsonResponse(
            {"error": "Customer does not exist"},
            status=404
        )

@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales_list = Sale.objects.all()
        return JsonResponse(
            {"sales_list": sales_list},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
#Automobile Entries
        try:
            vin_id = content["automobile"]["vin"]
            automobile = AutomobileVO.objects.get(vin=vin_id)
            if automobile.sold is False:
                AutomobileVO.objects.filter(vin=vin_id).update(sold=True)

                content["automobile"] = automobile
            else:
                return JsonResponse(
                    {"message": "Automobile has been sold!"},
                    status=400,
                )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "VIN does not exist"},
                status=404,
            )
#Customer Entries
        try:
            address_entry = content["customer"]["address"]
            customer = Customer.objects.get(address=address_entry)
            content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid address!"},
                status=404,
            )
#Salesperson Entries
        try:
            employee_id_entry = content["salesperson"]["employee_id"]
            salesperson = Salesperson.objects.get(employee_id=employee_id_entry)
            content["salesperson"] = salesperson
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid Employee ID"},
                status=404,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
             sale,
             encoder=SaleEncoder,
             safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    try:
        sales = Sale.objects.get(id=id)
        sales.delete()
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )
    except Sale.DoesNotExist:
        return JsonResponse(
            {"error": "Sale not found!"},
            status=404,
        )

@require_http_methods(["GET"])
def api_sales_person_history(request, id):
    if request.method == "GET":
        content = json.loads(request.body)
        employee_id_entry = content["salesperson"]["employee_id"]
        salespersons_history = Salesperson.objects.get(employee_id=employee_id_entry)
        return JsonResponse(
            {"salespersons_history": salespersons_history},
            encoder=SalespersonEncoder,
        )
