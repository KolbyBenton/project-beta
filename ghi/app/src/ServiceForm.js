import React, { useState, useEffect } from 'react';

const ServiceForm = () => {
  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date_time: '',
    technician: '',
    reason: '',
  });

  const [technicians, setTechnicians] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const serviceUrl = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(serviceUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const fetchData = async () => {
    const techniciansUrl = 'http://localhost:8080/api/technicians/';
    try {
      const response = await fetch(techniciansUrl);

      if (response.ok) {
        const data = await response.json();
        console.log('Technicians data:', data);
        setTechnicians(data.technicians);
      } else {
        console.error('Error fetching technicians:', response.status);
      }
    } catch (error) {
      console.error('Error fetching technicians:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.customer}
                placeholder="customer"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.date_time}
                placeholder="date_time"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time">Date Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.technician}
                required
                name="technician"
                id="technician"
                className="form-control"
              >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => (
                  <option key={technician.employee_id} value={technician.employee_id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.reason}
                placeholder="reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ServiceForm;
