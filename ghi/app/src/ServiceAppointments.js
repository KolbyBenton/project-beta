import React, { useState, useEffect } from 'react';

const ServiceAppointments = () => {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const cancelAppointment = async (vin) => {
    const url = `http://localhost:8080/api/appointments/${vin}/cancel/`;

    try {
      const response = await fetch(url, { method: 'PUT' });

      if (response.ok) {
        setAppointments(appointments.filter(appointment => appointment.vin !== vin));
      } else {
        console.error('failed to cancel appointment:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('error canceling appointment:', error);
    }
  };

  const finishAppointment = async (vin) => {
    const url = `http://localhost:8080/api/appointments/${vin}/finish/`;

    try {
      const response = await fetch(url, { method: 'PUT' });

      if (response.ok) {
        setAppointments(appointments.filter(appointment => appointment.vin !== vin));
      } else {
        console.error('failed to finish appointment:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('error finishing appointment:', error);
    }
  };

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date & Time</th>
            <th>Technician ID</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician_id}</td>
              <td>{appointment.reason}</td>
              <td>
                <button
                  onClick={() => cancelAppointment(appointment.vin)}
                  style={{ backgroundColor: 'red', color: 'white' }}
                >
                  Cancel
                </button>
                <button
                  onClick={() => finishAppointment(appointment.vin)}
                  style={{ backgroundColor: 'green', color: 'white' }}
                >
                  Finish
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ServiceAppointments;
