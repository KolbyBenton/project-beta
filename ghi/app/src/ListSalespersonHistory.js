import React, { useEffect, useState } from 'react';

const SalespersonHistoryList = () => {
  const [sales1, setSales] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales_list);
    }
  }



  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
        <h1>Salesperson History</h1>
        <select style={{width: '940px' }} value={sales1} onChange={e => setSales(e.target.value)}>
          <option value="">Select a Salesperson</option>
          {sales1.map(sale => (
            <option key={sale.automobile.vin} value={sale.automobile.vin}>
              {sale.salesperson.first_name} {sale.salesperson.last_name}
            </option>
          ))}
        </select>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales1.map(sale => (
          <tr key={sale.automobile.vin}>
            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
          </tr>
        ))}
      </tbody>
    </table>
    </div>
  );
};
export default SalespersonHistoryList;
