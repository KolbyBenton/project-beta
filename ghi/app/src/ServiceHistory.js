import React, { useState, useEffect } from 'react';

const ServiceHistory = () => {
  const [vin, setVin] = useState('');
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearch = (e) => {
    e.preventDefault();

    const filteredAppointments = appointments.filter(
      (appointment) => appointment.vin === vin
    );

    setAppointments(filteredAppointments);
  };

  return (
    <div>
      <h1>Service History</h1>
      <form onSubmit={handleSearch}>
        <label>
          VIN:
          <input
            type="text"
            value={vin}
            onChange={(e) => setVin(e.target.value)}
          />
        </label>
        <button type="submit">Search</button>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Name</th>
            <th>Date & Time</th>
            <th>Technician ID</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.vin}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician_id}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ServiceHistory;
