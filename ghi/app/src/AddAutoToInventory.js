import React, {useState, useEffect } from 'react';

const AddAutoInventory = () => {
  const [formData, setFormData] = useState({
      color: '',
      year: '',
      vin: '',
  });

  const [models, setModels] = useState([]);

  const handleSubmit = async(event) =>{
      event.preventDefault();

      const Url='http://localhost:8100/api/automobiles/'


      const fetchConfig = {
          method:"post",
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
          },
      };

  const response = await fetch(Url, fetchConfig);

  if (response.ok){
      setFormData({
          color: '',
          year: '',
          vin: '',
      });
  };
};

  const handleFormChange = (e) =>{
      const value = e.target.value;
      const inputName=e.target.name;
      setFormData({

          ...formData,

          [inputName]: value
      }

      );
  }
  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-auto-inventory-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.color}
                    placeholder="color"
                    required type="text"
                    name="color"
                    id="color"
                    className="form-control"
                    />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.year}
                    placeholder="year"
                    required type="text"
                    name="year"
                    id="year"
                    className="form-control"
                    />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.vin}
                    placeholder="vin"
                    required type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                    />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div>
                    <select style={{width: '410px' }}  onChange={handleFormChange}>
                        <option value="">Choose a Model</option>
                        {models.map(model => (
                            <option key={model.name} value={model.name}>
                                {model.name}
                            </option>
                        ))}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>

);

}

export default AddAutoInventory;
