import React, { useEffect, useState } from 'react';

const ModelList = () => {
  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(vehiclemodel => (
            <tr key={vehiclemodel.name}>
              <td>{vehiclemodel.name}</td>
              <td>{vehiclemodel.manufacturer.name}</td>
              <td>
                <img
                  src={vehiclemodel.picture_url}
                  alt="Model"
                  style={{ maxWidth: '200px', maxHeight: '200px' }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ModelList;
