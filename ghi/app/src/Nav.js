import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
            <NavLink className="nav-link" to="/technicianform">Technician Form</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/technicianslist">Technicians List</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/manufacturerlist">Manufacturer List</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/manufacturerform">Manufacturer Form</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/serviceform">Create Appointment</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/serviceappointments">Service Appointments</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/servicehistory">Service History</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/modellist">Models</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link"to="/salespeople">Salespeople</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/salespeople/create">Add a Salesperson</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/customers">Customers</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/customers/create">Add a Customer</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/sales">Sales</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/sales/create">Add a Sale</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/sales/history">Sales History</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/automobiles">Automobiles</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/models/create">Create a vehicle model</NavLink>
          </li>
          <li>
            <NavLink className="nav-link"to="/automobiles/create">Create an Automobile</NavLink>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
