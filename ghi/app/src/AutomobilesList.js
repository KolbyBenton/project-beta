import React, { useEffect, useState } from 'react';

const AutomobilesList = () => {
  const [automobile1, setAutomobile] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobile(data.autos);
    }
  }



  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
        <h1>Automobiles</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobile1.map(autos => (
          <tr key={autos.vin}>
            <td>{autos.vin}</td>
            <td>{autos.color}</td>
            <td>{autos.year}</td>
            <td>{autos.model.name}</td>
            <td>{autos.model.manufacturer.name}</td>
            <td>{autos.sold ? 'Yes' : 'No'}</td>
          </tr>
        ))}
      </tbody>
    </table>
    </div>
  );
};
export default AutomobilesList;
