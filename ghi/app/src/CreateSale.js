import React, {useState, useEffect } from 'react';

const CreateSale = () => {
  const [formData, setFormData] = useState({
      vin: '',
      salesperson: '',
      customer: '',
      price: '',
  });

  const handleSubmit = async(event) =>{
      event.preventDefault();

      const createnewsaleUrl='http://localhost:8090/api/sales/'


      const fetchConfig = {
          method:"post",
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
          },
      };

  const response = await fetch(createnewsaleUrl, fetchConfig);

  if (response.ok){
      setFormData({
          vin: '',
          salesperson: '',
          customer: '',
          price: '',
      });
  };
};

  const handleFormChange = (e) =>{
      const value = e.target.value;
      const inputName=e.target.name;
      setFormData({

          ...formData,

          [inputName]: value
      }

      );
  }
  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a New Sale</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.first_name}
                    placeholder="vin"
                    required type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                    />
                    <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.salesperson}
                    placeholder="salesperson"
                    required type="text"
                    name="salesperson"
                    id="salesperson"
                    className="form-control"
                    />
                    <label htmlFor="salesperson">Salesperson</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.customer}
                    placeholder="customer"
                    required type="text"
                    name="customer"
                    id="customer"
                    className="form-control"
                    />
                    <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.address}
                    placeholder="price"
                    required type="text"
                    name="price"
                    id="price"
                    className="form-control"
                    />
                    <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>

);

}

export default CreateSale;
