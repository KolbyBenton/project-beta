import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './AddSalesPerson';
import SalesPeopleList from './ListAllSalesPeople';
import AddCustomer from './AddCustomer';
import CreateSale from './CreateSale';
import SalesList from './ListAllSales';
import SalespersonHistoryList from './ListSalespersonHistory';
import ListAllCustomers from './ListAllCustomers';
import CreateVehicleModel from './CreateVehicleModel';
import AutomobilesList from './AutomobilesList';
import AddAutoInventory from './AddAutoToInventory';

import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ServiceForm from './ServiceForm';
import ServiceAppointments from './ServiceAppointments';
import ServiceHistory from './ServiceHistory';
import ModelList from './ModelsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path='/technicianform' element={<TechnicianForm />} />
          <Route path='/technicianslist' element={<TechniciansList />} />
          <Route path='/manufacturerlist' element={<ManufacturerList />} />
          <Route path='/manufacturerform' element={<ManufacturerForm />} />
          <Route path='/serviceform' element={<ServiceForm />} />
          <Route path='/serviceappointments' element={<ServiceAppointments />} />
          <Route path='/servicehistory' element={<ServiceHistory />} />
          <Route path='/modellist' element={<ModelList />} />
          <Route path="/salespeople/create" element={<SalespersonForm />} />
          <Route path="/salespeople/" element={<SalesPeopleList />} />
          <Route path="/customers/create" element={<AddCustomer />} />
          <Route path="/sales/create" element={<CreateSale />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/history" element={<SalespersonHistoryList />} />
          <Route path="/customers/" element={<ListAllCustomers />} />
          <Route path="/models/create" element={<CreateVehicleModel />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/create" element={<AddAutoInventory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
