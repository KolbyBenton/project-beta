import React, {useState, useEffect } from 'react';

const CreateVehicleModel = () => {
  const [formData, setFormData] = useState({
      name: '',
      picture_url: '',
      manufacturer: '',
  });

  const [manufacturers, setManufacturers] = useState([]);

  const handleSubmit = async(event) =>{
      event.preventDefault();

      const Url='http://localhost:8100/api/models/'


      const fetchConfig = {
          method:"post",
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
          },
      };

  const response = await fetch(Url, fetchConfig);

  if (response.ok){
      setFormData({
          name: '',
          picture_url: '',
          manufacturer: '',
      });
  };
};

  const handleFormChange = (e) =>{
      const value = e.target.value;
      const inputName=e.target.name;
      setFormData({

          ...formData,

          [inputName]: value
      }

      );
  }
  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.name}
                    placeholder="name"
                    required type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    />
                    <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange}
                    value={formData.picture_url}
                    placeholder="picture_url"
                    required type="text"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                    />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div>
                    <select style={{width: '410px' }}  onChange={handleFormChange}>
                        <option value="">Choose a Manufacturer</option>
                        {manufacturers.map(manufacturer => (
                            <option key={manufacturer.name} value={manufacturer.name}>
                                {manufacturer.name}
                            </option>
                        ))}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>

);

}

export default CreateVehicleModel;
