from django.urls import path
from .views import (
    api_technicians,
    api_appointments,
    api_AutomobileVO,
    api_technician,
    api_appointment
)

urlpatterns = [
    path('technicians/', api_technicians, name='api_technicians'),
    path('appointments/', api_appointments, name='api_appointments'),
    path('autmobolievo/', api_AutomobileVO, name='api_AutomobileVO'),
    path("technicians/<str:employee_id>/",
         api_technician,
         name="api_technician"
         ),
    path("appointments/<str:vin>/",
         api_appointment,
         name="api_appointment"
         ),
    path("appointments/<str:vin>/finish/",
         api_appointment,
         name="api_appointment"
         ),
    path("appointments/<str:vin>/cancel/",
         api_appointment,
         name="api_appointment"
         ),
]
